import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.util.Base64;

public class PublicKeyReader {

    public static PublicKey get(String filename, String algorithm)
            throws Exception {

        File f = new File(filename);
        FileInputStream fis = new FileInputStream(f);
        DataInputStream dis = new DataInputStream(fis);
        byte[] keyBytes = new byte[(int)f.length()];
        dis.readFully(keyBytes);
        dis.close();

        X509EncodedKeySpec spec =
                new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance(algorithm);
        return kf.generatePublic(spec);
    }

    public static PublicKey getFromString(String key, String algorithm) throws Exception {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] keyBytes = key.getBytes();
        keyBytes = decoder.decode(keyBytes);
        X509EncodedKeySpec spec =
                new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance(algorithm);
        return kf.generatePublic(spec);
    }
}