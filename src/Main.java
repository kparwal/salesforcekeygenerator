import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.util.Base64;



public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.out.println("USAGE: java SFInstallerKeyGenerator [company name] [expiration date YYYYMMDD]");
            return;
        }
        PrivateKey privateKeyDSA = PrivateKeyReader.get("private_dsa_key.der", "DSA");
        PublicKey publicKeyDSA;
        PublicKey publicKeyRSA = PublicKeyReader.get("public_key.der", "RSA");
        PrivateKey privateKeyRSA = PrivateKeyReader.get("private_key.der", "RSA");
        //<editor-fold desc="Java Key Generation (unused)">
//        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA", "SUN");
//        SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
//        keyGen.initialize(1024, random);
//        KeyPair pair = keyGen.generateKeyPair();
//        PrivateKey privateKey = pair.getPrivate();
//        PublicKey publicKey = pair.getPublic();
        //</editor-fold>

        //<editor-fold desc="License JSON Generation">
        int rawDate = 0;
        try {
            rawDate = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            System.err.println("This must be a date in the format YYYYMMDD");
            System.exit(1);
        }
        String name = args[1];
        if (name.length() > 15) {
            name = name.substring(0, 15);
        }
        String licenseJSON = "{\"EXP\":\"" + rawDate +"\",\"NAME\":\"" + name + "\"}";
        byte [] bytes = licenseJSON.getBytes("UTF-8");
        //</editor-fold>

        //<editor-fold desc="Signing JSON file">
        Base64.Encoder base64encoder = Base64.getEncoder();

        FileInputStream publicDSAKeyInputStream = new FileInputStream("public_dsa_key.der");
        byte[] publicKeyBytes = new byte [publicDSAKeyInputStream.available()];
        publicDSAKeyInputStream.read(publicKeyBytes);
        byte[] publicKeyBytesEncoded = base64encoder.encode(publicKeyBytes);
        String publicKeyString = new String(publicKeyBytesEncoded);

        Signature signature = Signature.getInstance("SHA1withDSA", "SUN");
        signature.initSign(privateKeyDSA);
        signature.update(bytes);
        byte[] realSignature = signature.sign();
        byte [] encodedSignature = base64encoder.encode(realSignature);
        String signatureString = new String(encodedSignature);
        String fullJSON = "{\n\"license\":"+ licenseJSON + ",\n\"signature\": \"" + signatureString + "\",\n"
                + "\"public_key\":" + "\"" + publicKeyString + "\"\n}";
//        File output = new File("digital_signature.signature");
//        FileOutputStream fileOutputStream = new FileOutputStream(output);
//        fileOutputStream.write(realSignature);
        File output = new File("unique_data.json");
        FileOutputStream fileOutputStream = new FileOutputStream(output);
        fileOutputStream.write(fullJSON.getBytes());

        //</editor-fold

        //<editor-fold desc="Verify License">
//        DataInputStream fileInputStream = new DataInputStream(new FileInputStream(new File("digital_signature.signature")));
//        fileInputStream.readFully(realSignature);
        DataInputStream fileInputStream = new DataInputStream(new FileInputStream(new File("unique_data.json")));
        byte[] JSON = new byte[fileInputStream.available()];
        fileInputStream.read(JSON);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(new String(JSON));
        String license = jsonNode.get("license").toString();
        publicKeyString = jsonNode.get("public_key").toString();
        publicKeyString = publicKeyString.substring(1, publicKeyString.length()-1);
        System.out.println(publicKeyString);
        publicKeyDSA = PublicKeyReader.getFromString(publicKeyString, "DSA");
        Signature verifier = Signature.getInstance("SHA1withDSA", "SUN");
        verifier.initVerify(publicKeyDSA);
        verifier.update(license.getBytes());
        fileInputStream.close();
        fileOutputStream.close();
        System.out.println("License file 'unique_data.json' created successfully.");
        //</editor-fold>
    }
}